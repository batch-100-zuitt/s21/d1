

// for user interface
const express = require ('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');	
//require and configure .env
require('dotenv').config()

//connect to mongoDB
const connectionString = process.env.DB_CONNECTION_STRING
mongoose.connect(  connectionString,
       {
       useNewUrlParser: true,
       useUnifiedTopology: true,
       useFindAndModify: false});
mongoose.connection.once('open', () => console.log(' Now connected to MongoDB Atlas.'));
  
       

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded( {extended: true } ));



//Routes 
app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);

//save environment variables
const port = process.env.PORT;
app.listen(port, () => console.log(`You got served on port ${port}.`));

