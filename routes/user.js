const express = require('express');
//creates a new router object

const router = express.Router();
const UserController = require('../controllers/user');

//create a route for getting details of a user
const auth = require('../auth');

//create a route if email exist
router.post('/email-exists', (req, res) =>  {
	UserController.emailExists(req.body)
	.then(result => res.send(result));

});

//create a route to a register a user
router.post('/', (req, res) => {
	UserController.register(req.body)
	.then(result => res.send(result));
});

//create a route to login a user
router.post('/login', (req, res) => {
    UserController.login(req.body)
    .then(result => res.send(result));
});

//create a route for getting details of a user
//router.get(route, middleware, callback) 		
router.get('/details', auth.verify, (req,res) => {
	const user = auth.decode(req.headers.authorization)

    console.log(user)
	UserController.get({ userId: user.id })
	.then(user => res.send(user))
})



   //routes for enrollment. after gumawa ng schema eto muna
  router.post('/enroll', auth.verify,(req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}  
     UserController.enroll(params).then(result => res.send(result))
})

  //miscellaneous
  router.put('details', (req, res) => {
  	UserController.updateDetails()
  })

  router.put('/change-password', (req, res) => {
  	userController.changePassword()
  })

//exports the router object
module.exports = router;


